/* filesys.h - Defines for setup of file system
 * vim:ts=4 noexpandtab
 */

#ifndef _FILESYS_H
#define _FILESYS_H

#include "types.h"
#include "syscall.h"

#define F_NAME_SIZE 32

typedef struct data_block {
	uint8_t data[4096];
} data_block_t;

typedef struct inode {
	uint32_t f_bsize;
	uint32_t data_block_num[1023];
} inode_t;

typedef struct dentry {
	uint8_t f_name[F_NAME_SIZE];
	uint32_t f_type;
	uint32_t num_inode;
	uint8_t reserved[24];
} dentry_t;

typedef struct boot_block {
	uint32_t num_dentries;
	uint32_t num_inodes;
	uint32_t num_data_blocks;
	uint8_t reserved[52];
	dentry_t dentries[63];
} boot_block_t;

void filesys_init(uint32_t filesys_addr);
int32_t read_dentry_by_name(const uint8_t* f_name, dentry_t* dentry);
int32_t read_dentry_by_index(uint32_t index, dentry_t* dentry);
int32_t read_data(uint32_t inode, uint32_t offset, uint8_t* buf, uint32_t length);
int32_t read_directory(uint8_t* buf, int32_t nbytes);
int32_t get_file_size_by_name(const uint8_t* f_name);
int32_t get_file_size_by_index(uint32_t index);
int32_t file_open(const uint8_t* f_name);
int32_t file_read(int32_t fd, void* buf, int32_t nbytes);
int32_t file_write(int32_t fd, const void* buf, int32_t nbytes);
int32_t file_close(int32_t fd);
int32_t dir_open(const uint8_t* f_name);
int32_t dir_read(int32_t fd, void* buf, int32_t nbytes);
int32_t dir_write(int32_t fd, const void* buf, int32_t nbytes);
int32_t dir_close(int32_t fd);

#endif /* _FILESYS_H */
