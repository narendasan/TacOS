/* keyboard.c - Set up keyboard
* vim:ts=4 noexpandtab
*/

#include "keyboard.h"

#define NUM_OFFSET 48
#define uint8_t_OFFSET 32
#define KEY_BKSP 14
#define KEY_ALT_DOWN 56
#define KEY_ALT_UP 184
#define F1 59
#define F2 60
#define F3 61
#define KEY_CAPS 58
#define KEY_CTRL_DOWN 29
#define KEY_SPACE 0x39
#define KEY_CHAR_LIMIT 53
#define KEY_SHIFT_DOWN 42
#define KEY_SHIFT_UP 170
#define KEY_CTRL_UP 157
#define KEY_RELEASED 0x80


static uint32_t ctrl = 0;
static uint32_t shift = 0;
static uint32_t caps = 0;

static uint32_t alt = 0;

volatile uint32_t key_flag = 0;
volatile uint8_t key_char = '\0';
volatile uint32_t clear_flag = 0;

uint8_t numToSym [10] = {')','!','@','#','$','%','^','&','*','('};
uint8_t key_press[128] =
{
    0,  27, '1', '2', '3', '4', '5', '6', '7', '8',	'9', '0', '-', '=', '\b',	/* Backspace */
    '\t',	/* Tab */ 'q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p', '[', ']', '\n',	/* Enter key */
    '@',/* Ctrl */'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', ';',	'\'', '`',   '@',		/* Left shift */
    '\\', 'z', 'x', 'c', 'v', 'b', 'n', 'm', ',', '.', '/',   '@',				/* Right shift */
    '*',
    '@',	/* Alt */
    ' ',	/* Space bar */
    '@',	/* Caps lock */
    '@',	/* 59 - F1 key ... > */
    '@',   '@',   '@',   '@',   '@',   '@',   '@',   '@',
    '@',	/* < ... F1'@' */
    '@',	/* 69 - Num lock*/
    '@',	/* Scroll Lock */
    '@',	/* Home key */
    '@',	/* Up Arrow */
    '@',	/* Page Up */
    '-',
    '@',	/* Left Arrow */
    '@',
    '@',	/* Right Arrow */
    '+',
    '@',	/* 79 - End key*/
    '@',	/* Page Down */
    '@',	/* Insert Key */
    '@',	/* Delete Key */
    '@',   '@',   '@',
    '@',	/* F11 Key */
    '@',	/* F12 Key */
    '@',	/* All other keys are undefined */
};



uint8_t symHandler(uint8_t c, uint32_t shift);
uint8_t uint8_tHandler(uint8_t c, uint32_t shift, uint32_t caps);
uint8_t numHandler(uint8_t c, uint32_t shift);
uint8_t visableHandler(uint8_t c, uint32_t shift);

/*
 * function: keyboard_check
 * params: none
 * returns: none
 * gets Scancode and then checks to see if it's a "hold-down key" e.g ctrl, alt, shift
 * also performs checks to see if keys that are held down are in conjunction with other keys for special functions
*/

void keyboard_check()
{
    uint8_t c = getScancode();
    if (c == KEY_BKSP)
    {
        key_char = '\b';
        key_flag = 1;
    }
    else if (c == KEY_CAPS)
    {
        caps = !caps;
    }
    else if (c == KEY_CTRL_DOWN)
    {
        ctrl = 1;
    }
    else if(c == KEY_CTRL_UP)
    {
        ctrl = 0;
    }
    else if (ctrl){
        uint8_t arg =  key_press[c];
        switch (arg)
        {
            case 'l':
                clear_term();
                key_char = '\n';
                key_flag = 1;
                break;
            default:
                break;
        }
        return;
    }
    else if ((c != KEY_SHIFT_DOWN) && ((c <= KEY_CHAR_LIMIT || c == KEY_SPACE)) && (c != 0))
    {
        uint8_t character = key_press[c];
        uint8_t text;
        text = visableHandler(character, shift);
        key_char = text;
        key_flag = 1;
    }
    else if (c == KEY_SHIFT_DOWN)
    {
        shift = 1;
    }
    else if (c == KEY_SHIFT_UP)
    {
        shift = 0;
    }
    if (c == KEY_ALT_DOWN)
    {
        alt = 1;
    }
    if (c == KEY_ALT_UP)
    {
        alt = 0;
    }
    if (alt)
    {
        switch(c)
        {
            case F1:
                switch_term(0);
                break;
            case F2:
                switch_term(1);
                break;
            case F3:
                switch_term(2);
                break;
            default:
                break;
        }
    }
}

/* function: getScancode
 * params: none
 * returns scancode
 * function gets a code from the keyboard port and then returns it
*/

uint8_t getScancode()
{
    uint8_t scancode;

    /* Read from the keyboard's data buffer */
    scancode = inb(KEYBOARD_PORT);
    // printf("%d", scancode );
    /* If the top bit of the byte we read from the keyboard is
    *  set, that means that a key has just been released */
    if (scancode & KEY_RELEASED)
    {
        /* shift, alt, or control keys... */
        return scancode;
    }
    else
    {
        return scancode;
    }

}

/* function: visableHandler
 * params: character and shift boolean
 * returns character
 * function is given a character and a shift boolean
 * and goes to one of three functions depending on the type of character it is
*/

uint8_t visableHandler(uint8_t uint8_tacter, uint32_t s){
        if ((uint8_tacter >= '0') && (uint8_tacter <= '9')){
            uint8_tacter = numHandler(uint8_tacter, s);
        }
        else if ((uint8_tacter >= 'a') && (uint8_tacter <= 'z')){
            uint8_tacter = uint8_tHandler(uint8_tacter, s, caps);
        }
        else {
            uint8_tacter = symHandler(uint8_tacter, s);
        }
        return uint8_tacter;

}

/* function: numHandler
 * params: character and shift
 * returns character
 * function is given an integer that represents a character (a number) and a shift boolean
 * if there's a shift the function returns the appropriate symbol, else it returns the number
*/

uint8_t numHandler(uint8_t c, uint32_t shift) {
    if (shift){
        int indx = (uint32_t) (c) - NUM_OFFSET;
        return numToSym[indx];
    }
    else {
        return c;
    }
}

/* function: uint8_tHandler
 * params: character, shift, caps
 * returns character
 * function is given an integer that represents a character (a number) and a shift boolean and caps boolean
 * if there's a shift or caps the function returns the appropriate uint8_t representation capitalized or non-capitalized letter
*/

uint8_t uint8_tHandler(uint8_t c, uint32_t shift, uint32_t caps){
    if (shift || caps){
        return c - uint8_t_OFFSET;
    }
    else {
        return c;
    }
}

/* function: symHandler
 * params: character, shift
 * returns a character
 * function is given a integer that represents a character and a shift boolean
 * if there's a shift or caps the function returns the appropriate symbol, else it returns the uint8_t character
*/

uint8_t symHandler(uint8_t c, uint32_t shift){
    switch(c){
        case '`':
        if (shift){
            return '~';
        }
        case '-':
        if (shift){
            return '_';
        }
        case '=':
        if (shift){
            return '+';
        }
        case '[':
        if (shift){
            return '{';
        }
        case ']':
        if (shift){
            return '}';
        }
        case '\\':
        if (shift){
            return '|';
        }
        case ';':
        if (shift){
            return ':';
        }
        case '\'':
        if (shift){
            return '\"';
        }
        case ',':
        if (shift){
            return '<';
        }
        case '.':
        if (shift){
            return '>';
        }
        case '/':
        if (shift){
            return '?';
        }
    }
    return c;
}
