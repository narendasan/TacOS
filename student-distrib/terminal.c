#include "terminal.h"

uint32_t current_term;
uint32_t service_term;
term_t terminals[NUM_TERM];
static uint32_t term_vid_addr[3] = {0x8401000,0x8402000,0x8403000};
uint32_t term_phys_vid_addr[3] = {0xBA000,0xBC000,0xBE000};

/*
init_terminal
function: starts the terminal and sets the backking pages
inputs:
outputs:
returns: none
*/

void init_terminal(){
	uint32_t i;
	for(i = 0; i < NUM_TERM; i++){
			terminals[i].screen_x = 0;
			terminals[i].screen_y = 0;
			terminals[i].curr_pcb = NULL;
			terminals[i].curr_PID = (i + 1) * 10;
			create_vid_page(term_phys_vid_addr[i],i+1);
	}
	clear();
	current_term = 0;
	service_term = 0;
	initShell(1);
	initShell(2);
}

/*
terminal_read
function: this function takes a buffer from the user and sits in a while loop until it sees
a return or newline character, and if it sees the keyboard raise a flag and it has access to the keyboard
it will read that character to the buffer and then mark the keystroke as serviced. Returns when you hit enter
inputs:  fd,  buf, nbytes
outputs:
returns: none
*/

int32_t terminal_read (int32_t fd, void* buf, int32_t nbytes){
	if(buf == NULL || nbytes < 0){
		return -1;
	}
	uint8_t c = '\0';
	uint32_t idx = 0;

	while((c != '\r' && c != '\n')){
		if (service_term != current_term)
			continue;
		if (key_flag){
			//STI
			c = key_char;
			if(c == '\b'){
				if(idx != 0){
					printf("%c",c);
					((uint8_t*)buf)[idx] = '\0';
					idx--;
				}
			} else {
				if (idx < nbytes && idx < BUFFER_LIMIT){
					((uint8_t*)buf)[idx] = key_char;
					idx++;
					printf("%c",c);
				}
			}
			key_flag = !key_flag;
			//CLI
		}
	}
	return idx;
}

/*
terminal_write
function: this function takes the buffer and prints it to the console
inputs:  fd,  buf, nbytes
outputs:
returns: none
*/

int32_t terminal_write(int32_t fd, const void* buf, int32_t nbytes){
	if(buf == NULL || nbytes < 0){
		return -1;
	}
	uint32_t i;
	for(i = 0; i < nbytes; i++){
		printf("%c", ((uint8_t*)buf)[i]);
	}
	return nbytes;
}

/*
terminal_open
function:
inputs: filename
outputs:
returns: none
*/

int32_t terminal_open(const uint8_t* filename)
{
	return 0;
}

/*
terminal_close
function:
inputs:
outputs:
returns: none
*/

int32_t terminal_close(int32_t fd)
{
	return 0;
}

/*
switch_term
function: swtiches video memory for terminals
inputs: terminal to swtich to
outputs: success of function
returns: none
*/

int32_t switch_term (uint32_t target_term)
{
	if(target_term == current_term){
		return -1;
	}
	if(target_term > 2 || target_term < 0){
		return -1;
	}
	cli();

	key_flag = 0;

	if (service_term != current_term)
		remap_vidmem(VIDEOPHYSADDR);
	memcpy((void *)term_vid_addr[current_term], (void *)VIDEO, SCREEN_SIZE);

	memcpy((void *)VIDEO, (void *)term_vid_addr[target_term], SCREEN_SIZE);
	update_cursor(terminals[target_term].screen_x,terminals[target_term].screen_y);
	if (service_term != target_term)
		remap_vidmem(term_phys_vid_addr[service_term]);

	if (service_term != target_term)
	 	create_vid_page(term_phys_vid_addr[service_term],0);
	else
	 	create_vid_page(VIDEOPHYSADDR,0);

	current_term = target_term;

	sti();

	return 0;
}

/*
clear_term
function: clears current term video mem
inputs:
outputs:
returns: none
*/

void clear_term ()
{
	cli();

	if (service_term != current_term)
	{
		terminals[service_term].screen_x = screen_x;
		terminals[service_term].screen_y = screen_y;
		screen_x = terminals[current_term].screen_x;
		screen_y = terminals[current_term].screen_y;
		remap_vidmem(VIDEOPHYSADDR);
	}
	clear();
	if (service_term != current_term)
	{
		remap_vidmem(term_phys_vid_addr[service_term]);
		terminals[current_term].screen_x = screen_x;
		terminals[current_term].screen_y = screen_y;
		screen_x = terminals[service_term].screen_x;
		screen_y = terminals[service_term].screen_y;
	}

	sti();
}
