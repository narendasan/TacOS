#ifndef _SYSCALL_H
#define _SYSCALL_H

#include "lib.h"
#include "types.h"
#include "x86_desc.h"
#include "paging.h"

#define PCBNUMFILES 8
#define PRGMIMGVADDR 0x08000000
#define PRGMIMGVOFFSET 0x00048000
#define STDIN 0
#define STDOUT 1
#define TASKSTACK 0x083FFFFC
#define KERNELSTACK 0x00800000

typedef int32_t (*open_op)(const uint8_t*);
typedef int32_t (*read_op)(int32_t, void*, int32_t);           // defining new tupes for the jumptable
typedef int32_t (*write_op)(int32_t, const void*, int32_t);
typedef int32_t (*close_op)(int32_t);

typedef struct jump_table {
	open_op open_func;
	read_op read_func;
	write_op write_func;
	close_op close_func;
} jump_table_t;


typedef struct file {
	jump_table_t* file_op;
	uint32_t inode_ptr;
	uint32_t file_pos;
	uint32_t flags;
} file_t;


typedef struct pcb {
	file_t file_array[PCBNUMFILES];
	struct pcb_t* parent_task;
	uint32_t PID;
	uint32_t parent_esp;
	uint32_t parent_ebp;
	uint32_t curr_esp;
	uint32_t curr_ebp;
	uint8_t args[128];
} pcb_t;

int32_t open(const uint8_t* filename);
int32_t read(int32_t fd, void* buf, int32_t nbytes);
int32_t write(int32_t fd, const void* buf, int32_t nbytes);
int32_t close(int32_t fd);
int32_t execute (const uint8_t* command);
int32_t halt (uint8_t status);
int32_t getargs (uint8_t* buf, int32_t nbytes);
int32_t vidmap (uint8_t** screen_start);
int32_t set_handler (int32_t signum, void* handler_address);
int32_t sigreturn (void);
int32_t initShell(uint32_t term);

extern pcb_t* curr_task;

#endif /* _SYSCALL_H */
