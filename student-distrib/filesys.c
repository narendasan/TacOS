/* filesys.c - Set up file system
 * vim:ts=4 noexpandtab
 */

#include "filesys.h"
#include "lib.h"

static boot_block_t * boot_block_addr;
static inode_t * inodes_addr;
static data_block_t * data_blocks_addr;

/*
filesys_init
function: sets the bootblock , inode, and datablock addr
inputs: filesys_addr
outputs: 
returns:
*/

void filesys_init(uint32_t filesys_addr)
{
	boot_block_addr = (boot_block_t*)filesys_addr;
	inodes_addr = (inode_t*)(filesys_addr + sizeof(boot_block_t));
	data_blocks_addr = (data_block_t*)(filesys_addr + sizeof(boot_block_t) + boot_block_addr->num_inodes * sizeof(inode_t));
}

/*
read_dentry_by_name
function: checks to see if the f name passed in is a valid dentry
if the name is valid, sets type and inode number of the dentry passed in to
the dentry that corresponds to the name 
inputs: f_name, dentry
outputs: 
returns: -1 on failure and 0 on success
*/

int32_t read_dentry_by_name(const uint8_t* f_name, dentry_t* dentry)
{
	uint32_t name_size;
	name_size = strlen((int8_t*)f_name);
	if (name_size < 1 || name_size > F_NAME_SIZE - 1)
		return -1;

	uint32_t i;
	dentry_t temp;
	for (i = 0; i < boot_block_addr->num_dentries; i++)
	{
		temp = boot_block_addr->dentries[i];
		if (!strncmp((int8_t*)f_name, (int8_t*)temp.f_name, name_size + 1))
		{
			strncpy((int8_t*)(dentry->f_name), (int8_t*)(temp.f_name), F_NAME_SIZE);
			dentry->f_type = temp.f_type;
			dentry->num_inode = temp.num_inode;
			return 0;
		}
	}
	return -1;
}

/*
read_dentry_by_index
function: checks to see if the index passed in is a valid dentry
if the index is valid, sets type and inode number of the dentry passed in to
the dentry that corresponsd to the index 
inputs: f_name, dentry
outputs: 
returns: -1 on failure and 0 on success
*/

int32_t read_dentry_by_index(uint32_t index, dentry_t* dentry)
{
	if (index >= boot_block_addr->num_dentries)
		return -1;

	dentry_t temp;
	temp = boot_block_addr->dentries[index];
	strncpy((int8_t*)(dentry->f_name), (int8_t*)(temp.f_name), F_NAME_SIZE);
	dentry->f_type = temp.f_type;
	dentry->num_inode = temp.num_inode;
	return 0;
}

/*
read_data
function: reads a length of data and stores into a buf 
inputs:  inode, offset,  buf, length
outputs: 
returns: -1 on failure and length on success
*/

int32_t read_data(uint32_t inode, uint32_t offset, uint8_t* buf, uint32_t length)
{
	if (inode >= boot_block_addr->num_inodes)
		return -1;
	if (offset >= inodes_addr[inode].f_bsize){
		memset(buf , 0, length);
		return 0;
	}
	if (offset + length > inodes_addr[inode].f_bsize)
		length = inodes_addr[inode].f_bsize - offset;

	uint32_t i;
	uint32_t block_num;
	uint32_t block_offset;
	for (i = 0; i < length; i++)
	{
		block_num = (offset + i) / sizeof(data_block_t);
		block_offset = (offset + i) % sizeof(data_block_t);
		buf[i] = data_blocks_addr[inodes_addr[inode].data_block_num[block_num]].data[block_offset];
	}
	return length;
}

/*
get_file_size_by_name
function: takes a file name and returns the file size with tht name 
inputs:  f_name
outputs: 
returns: -1 on failure and length on success
*/

int32_t get_file_size_by_name(const uint8_t* f_name)
{
	dentry_t temp;
	if (read_dentry_by_name(f_name,&temp)==-1)
		return -1;
	return inodes_addr[temp.num_inode].f_bsize;
}

/*
get_file_size_by_index
function: takes a index and returns the file size with tht index 
inputs: index
outputs: 
returns: -1 on failure and length on success
*/

int32_t get_file_size_by_index(uint32_t index)
{
	dentry_t temp;
	if (read_dentry_by_index(index,&temp)==-1)
		return -1;
	return inodes_addr[temp.num_inode].f_bsize;
}

/*
file_open
function: nothing
inputs:  f_name
outputs: 
returns: returns 0 
*/

int32_t file_open(const uint8_t* f_name)
{
	return 0;
}

/*
file_read
function: checks if the file is read-able
inputs: fd, buf,  nbytes
outputs: 
returns: -1 on failure and data read return value on success
*/

int32_t file_read(int32_t fd, void* buf, int32_t nbytes)
{
	if (fd < 2 || fd > 7)
		return -1;
	uint32_t data_read;
	data_read = read_data(curr_task->file_array[fd].inode_ptr, curr_task->file_array[fd].file_pos, buf, nbytes);
	if (data_read == -1)
		return -1;
	curr_task->file_array[fd].file_pos += data_read;
	return data_read;
}

/*
file_write
function: nothing 
inputs: fd, buf,  nbytes
outputs: 
returns: -1  
*/

int32_t file_write(int32_t fd, const void* buf, int32_t nbytes)
{
	return -1;
}

/*
file_close
function: nothing 
inputs:  fd
outputs: 
returns: 0
*/

int32_t file_close(int32_t fd)
{
	return 0;
}

/*
dir_open
function: nothing 
inputs: f_name
outputs: 
returns: 0
*/

int32_t dir_open(const uint8_t* f_name)
{
	return 0;
}

/*
dir_read
function: given a fd , copies the name of the dentry to a buffer 
inputs: fd, buf,  nbytes
outputs: 
returns: the length of the buf
*/

int32_t dir_read(int32_t fd, void* buf, int32_t nbytes)
{
	if (fd < 2 || fd > 7)
		return -1;
	uint32_t curr_dentry = curr_task->file_array[fd].file_pos;
	if (curr_dentry >= boot_block_addr->num_dentries)
	{
		curr_task->file_array[fd].file_pos = 0;
		return 0;
	}
	strncpy((int8_t*)(buf), (int8_t*)(boot_block_addr->dentries[curr_dentry].f_name), nbytes);
	curr_task->file_array[fd].file_pos++;
	return strlen((int8_t*)buf);
}

/*
dir_write
function: nothing 
inputs: fd, buf,  nbytes
outputs: 
returns: -1 
*/

int32_t dir_write(int32_t fd, const void* buf, int32_t nbytes)
{
	return -1;
}

/*
dir_close
function: nothing 
inputs: fd
outputs: 
returns: 0
*/

int32_t dir_close(int32_t fd)
{
	return 0;
}
