#include "scheduling.h"

static uint32_t second_term = 1;
static uint32_t third_term = 1;

/*
scheduling
function: rempas video memory while also doing a task switch between terminals
inputs: 
outputs: 
returns: 0
*/

void scheduling() {
	uint32_t next_service = (service_term + 1) % NUM_TERM;

	//setting screen posisition
	terminals[service_term].screen_x = screen_x;
	terminals[service_term].screen_y = screen_y;

	screen_x = terminals[next_service].screen_x;
	screen_y = terminals[next_service].screen_y;

	//remapping terminal video page
	if (next_service != current_term)
		remap_vidmem(term_phys_vid_addr[next_service]);
	else
		remap_vidmem(VIDEOPHYSADDR);

	curr_task->curr_esp = pit_esp;
	curr_task->curr_ebp = pit_ebp;
	/* asm volatile("movl %%esp, %0     \n 	\
	 			movl %%ebp, %1"
	 			:"=r"(curr_task->curr_esp), "=r"(curr_task->curr_ebp)
	 			:
	 			:"memory");*/

	//setting user page
	curr_task = (pcb_t*)terminals[next_service].curr_pcb;
	uint32_t task_addr = EIGHTMBADDR + FOURMBADDR * terminals[next_service].curr_PID;
	create_task_page(task_addr);

	//remapping user video page
	if (next_service != current_term)
	 	create_vid_page(term_phys_vid_addr[next_service],0);
	else
	 	create_vid_page(VIDEOPHYSADDR,0);

	tss.ss0 = KERNEL_DS;
	tss.esp0 = EIGHTMBADDR - EIGHTKB * terminals[next_service].curr_PID - 4;

	service_term = next_service;

	if (second_term && next_service == 1)
	{

		second_term = 0;
		send_eoi(0);
		asm volatile("movl %0, %%esp    \n 	\
				movl %1, %%ebp"
				:
				:"r"(curr_task->curr_esp), "r"(curr_task->curr_ebp)
				:"memory");
		asm volatile("iret");
	}
	if (third_term && next_service == 2)
	{
		third_term = 0;
		send_eoi(0);
		asm volatile("movl %0, %%esp    \n 	\
				movl %1, %%ebp"
				:
				:"r"(curr_task->curr_esp), "r"(curr_task->curr_ebp)
				:"memory");
		asm volatile("iret");
	}

	/* asm volatile("movl %0, %%esp    \n 	\
	 			movl %1, %%ebp"
	 			:
	 			:"r"(curr_task->curr_esp), "r"(curr_task->curr_ebp)
	 			:"memory"); */

	pit_esp = curr_task->curr_esp;
	pit_ebp = curr_task->curr_ebp;
}
