#if !defined(SUPPORT_H)
#define SUPPORT_H

extern uint32_t strlen (const uint8_t* s);
extern void strcpy (uint8_t* dst, const uint8_t* src);
extern void fdputs (int32_t fd, const uint8_t* s);
extern int32_t strcmp (const uint8_t* s1, const uint8_t* s2);
extern int32_t strncmp (const uint8_t* s1, const uint8_t* s2,
			       uint32_t n);

#endif /* SUPPORT_H */
