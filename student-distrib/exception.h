/* exception.c - Defines for exceptions
 * vim:ts=4 noexpandtab
 */

#ifndef _EXCEPTION_H
#define _EXCEPTION_H

#include "types.h"
#include "lib.h"
#include "syscall.h"

void de_exception();
void db_exception();
void nmi_exception();
void bp_exception();
void of_exception();
void br_exception();
void ud_exception();
void nm_exception();
void df_exception();
void ts_exception();
void np_exception();
void ss_exception();
void gp_exception();
void pf_exception();
void mf_exception();
void ac_exception();
void mc_exception();
void xf_exception();
void rs_exception();

#endif /* _IDT_H */
