/* idt.h - Defines for setup of interrupt descriptor table
 * vim:ts=4 noexpandtab
 */

#ifndef _IDT_H
#define _IDT_H

#include "types.h"

void idt_init();

//found in asmlink.S
extern void keyboard_handler();
extern void rtc_handler();
extern void syscall_handler();
extern void pit_handler();

#endif /* _IDT_H */
