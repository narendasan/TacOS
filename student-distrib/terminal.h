/* terminal.c - Define for term
 * vim:ts=4 noexpandtab
 */

#ifndef _TERMINAL_H
#define _TERMINAL_H

#include "types.h"
#include "lib.h"
#include "syscall.h"
#include "i8259.h"
#include "paging.h"

#define BUFFER_LIMIT 128
#define SCREEN_LIMIT 3
#define SCREEN_X 80
#define SCREEN_Y 25
#define SCREEN_SIZE 4096
#define NUM_TERM 3

extern volatile uint32_t key_flag;
extern volatile uint8_t key_char;

void init_terminal();
int32_t terminal_read (int32_t fd, void* buf, int32_t nbytes);
int32_t terminal_write(int32_t fd, const void* buf, int32_t nbytes);
int32_t terminal_open(const uint8_t * filename);
int32_t terminal_close(int32_t fd);
int32_t switch_term (uint32_t target_term);
void clear_term();

typedef struct terminal {
    uint32_t screen_x;
    uint32_t screen_y;
    struct pcb_t * curr_pcb;
    int32_t curr_PID;
} term_t;

extern uint32_t current_term;
extern uint32_t service_term;
extern uint32_t term_phys_vid_addr[3];
extern term_t terminals[NUM_TERM];

#endif /* _TERMINAL_H */
