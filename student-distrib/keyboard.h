/* keyboard.c - Define for keyboard
 * vim:ts=4 noexpandtab
 */

#ifndef _KEYBOARD_H
#define _KEYBOARD_H

#include "types.h"
#include "lib.h"

void keyboard_check();
uint8_t getScancode();

#define KEYBOARD_PORT 0x60
#define KEYBOARD_ACK KEYBOARD_PORT + 1


#endif /* _KEYBOARD_H */
