#ifndef _SCHEDULING_H
#define _SCHEDULING_H

#include "types.h"
#include "syscall.h"
#include "terminal.h"
#include "i8259.h"

void scheduling();

extern uint32_t pit_esp;
extern uint32_t pit_ebp;

#endif
