#include "syscall.h"
#include "filesys.h"
#include "rtc.h"
#include "lib.h"
#include "terminal.h"



// R/W/OPEN /CLOSE STARTS HERE
// commented out for now

// theses jumptabl structs are loaded with the corresponding functions
jump_table_t rtc_table = {rtc_open , rtc_read, rtc_write, rtc_close};
jump_table_t directory_table = {dir_open, dir_read, dir_write, dir_close};
jump_table_t file_table = {file_open, file_read, file_write, file_close};
jump_table_t terminal_table = {terminal_open, terminal_read, terminal_write, terminal_close};


pcb_t* curr_task = NULL;
static pcb_t* prev_task = NULL;
static uint32_t shell_eip = 0;

/*
open
function: Sets the file array's file op , inode ptr , file pos , and flags based on the file type
if the filename is valid.
inputs: filename
outputs: 
returns: -1 if failure and 0 on success
*/

int32_t open(const uint8_t* filename)
{
	//check if filename is valid

	//find next open slot in the file array, by checking to see if the flgs are zero in each
	// file ararys fd's flag
	// if not move until you reach the end of file array
	//if you reach the end, then the file is cant be opened and open returns -1
	int fd = 2; //skip over stdin and out
	while (curr_task->file_array[fd].flags == 1)
	{
		fd++;
		if(fd > 7)
			return -1;
	}

	// when u find an open slot set the fd to the array number

	//call read_Dentry_by_name with the fd passed in
	// check if the return is -1 , if so return -1
	dentry_t temp;
	if (read_dentry_by_name(filename,&temp) == -1)
		return -1;

	// check the temp's file type
	// too see if it is a rtc, fiel , or directory
	// based on waht it is, we call our drivers, which can be an array of functions
	// you set the file pointer to point to the correct array
	//then you set the inode , file positon, and flags.
	// inode should be zero for rtc and directory and inode number from the dentry
	//file positon should be zero for start of file
	// flags should be set to 1 to show that it is in use

	// you then pick the open option i the array. and execute the correct open function


	switch(temp.f_type)
	{
		case 0:        //rtc
		if (rtc_table.open_func(filename) == -1)
			return -1;

		curr_task->file_array[fd].file_op = &rtc_table; //set the op to point to the correct table
		curr_task->file_array[fd].inode_ptr = NULL;
		curr_task->file_array[fd].file_pos = 0;
		curr_task->file_array[fd].flags = 1;
		return fd;

		case 1:         //directory
		if (directory_table.open_func(filename) == -1)
			return -1;

		curr_task->file_array[fd].file_op = &directory_table;
		curr_task->file_array[fd].inode_ptr = NULL;
		curr_task->file_array[fd].file_pos = 0;
		curr_task->file_array[fd].flags = 1;
		return fd;

		case 2:         //file
		if (file_table.open_func(filename) == -1)
			return -1;

		curr_task->file_array[fd].file_op = &file_table;
		curr_task->file_array[fd].inode_ptr = temp.num_inode;
		curr_task->file_array[fd].file_pos = 0;
		curr_task->file_array[fd].flags = 1;
		return fd;

		default:
			return -1;
	}

}

/*
read
function: calls the read function that corresponds to jumptable
if the filename is valid.
inputs: int32_t fd, void* buf, int32_t nbytes)
outputs: 
returns: -1 if failure and calls the read function on success
*/

int32_t read(int32_t fd, void* buf, int32_t nbytes)
{

	if (fd < 0 || fd > 7 || fd == 1 || buf == NULL || nbytes < 0)// check if fd is valid, cant do stdout when reading
		return -1;

	if (curr_task->file_array[fd].flags == 0)	// chek if the current pcb's file array's fd's flag  is in use
		return -1;

	//call the right read fnction
	// check the file array's fd's jump table ptr, pass the right values

	return curr_task->file_array[fd].file_op->read_func(fd, buf, nbytes);
}

/*
write
function: calls the write function that corresponds to jumptable
if the filename is valid.
inputs: int32_t fd, void* buf, int32_t nbytes)
outputs: 
returns: -1 if failure and calls the write function on success
*/

int32_t write(int32_t fd, const void* buf, int32_t nbytes)
{
	if (fd < 1 || fd > 7 || buf == NULL || nbytes < 0)// check if fd is valid, cant do stdin when writing
		return -1;

	if (curr_task->file_array[fd].flags == 0)	// chek if the current pcb's file array's fd's flag  is in use
		return -1;

	// call the correspoinding write function
	return curr_task->file_array[fd].file_op->write_func(fd, buf, nbytes);
}

/*
read
function: calls the read function that corresponds to jumptable
if the filename is valid.
inputs: int32_t fd
outputs: 
returns: -1 if failure and calls the close function on success
*/

int32_t close(int32_t fd)
{

	//check if the fd is valid
	// if not return -1
	if (fd < 2 || fd > 7)
		return -1;

	// check if the fd is even in the open state, by checking flags
	//if not return -1
	if (curr_task->file_array[fd].flags == 0)
		return -1;

	//then set the file positon to 0 , flag to 0 and the file_op to NULL
	int32_t stat = curr_task->file_array[fd].file_op->close_func(fd);
	curr_task->file_array[fd].file_op = NULL;
	curr_task->file_array[fd].inode_ptr = NULL;
	curr_task->file_array[fd].file_pos = 0;
	curr_task->file_array[fd].flags = 0;

	// call the flose function or just set the flags
	return stat;
}

/*
execute
function: given a command, the function gets the name and args in the command 
and checks if they are valid. If so then begins to check for ELF and gets the EIP
Sets up paging for the program and creates a new pcb. updates the pid and prev tasks
inputs: command
outputs: 
returns: -1 on failure and 0 on successs
*/

int32_t execute (const uint8_t* command)
{
	if (command == NULL)
		return -1;

	//finding file name
	uint32_t f_name_size = 0;
	while(command[f_name_size] != ' ' && command[f_name_size] != '\0' && command[f_name_size] != '\n')
		f_name_size++;
	uint8_t f_name[f_name_size+1];
	strncpy((int8_t*)(f_name), (int8_t*)(command), f_name_size);
	f_name[f_name_size] = '\0';

	//find arguments
	uint32_t args_size = 0;
	if (command[f_name_size] == ' ')
	{
		while(command[args_size+f_name_size+1] != '\0' && command[args_size+f_name_size+1] != '\n')
			args_size++;
	}
	uint8_t args[args_size+1];
	strncpy((int8_t*)(args), (int8_t*)(command) + f_name_size + 1, args_size);
	args[args_size] = '\0';

	//getting file dentry
	dentry_t temp_dentry;
	if(read_dentry_by_name(f_name, &temp_dentry) == -1)
		return -1;

	//checking ELF
	uint8_t ELF[4];
	if (read_data(temp_dentry.num_inode, 0, ELF, 4) == -1)
		return -1;

	if (ELF[0] != 0x7F || ELF[1] != 0x45 || ELF[2] != 0x4C || ELF[3] != 0x46)
		return -1;

	terminals[service_term].curr_PID += 1;

	//setting user page
	uint32_t task_addr = EIGHTMBADDR + FOURMBADDR * terminals[service_term].curr_PID;
	create_task_page(task_addr);

	//copy data to user page
	uint32_t task_eip = 0;
	if (read_data(temp_dentry.num_inode, 0, (uint8_t*)(PRGMIMGVADDR | PRGMIMGVOFFSET), FOURMB - PRGMIMGVOFFSET) == -1)
		return -1;
	if (read_data(temp_dentry.num_inode, 24, (uint8_t*)&task_eip, 4) == -1)
		return -1;

	//finding new pcb and stack
	prev_task = (pcb_t*)terminals[service_term].curr_pcb;
	uint32_t task_ebp = EIGHTMBADDR - EIGHTKB * terminals[service_term].curr_PID - 4;
	curr_task = (pcb_t*)(task_ebp & 0xFFFE000);
	curr_task->curr_ebp = task_ebp;

	//storing parent esp and ebp
	curr_task->PID = terminals[service_term].curr_PID;
	asm volatile("movl %%esp, %0     \n 	\
				movl %%ebp, %1"
				:"=r"(curr_task->parent_esp), "=r"(curr_task->parent_ebp)
				:
				:"memory");
	curr_task->parent_task = (struct pcb_t *)prev_task;
	terminals[service_term].curr_pcb = (struct pcb_t*)curr_task;
	strncpy((int8_t*)(curr_task->args), (int8_t*)args, args_size+1);

	//setting stdin
	curr_task->file_array[STDIN].file_op = &terminal_table;
	curr_task->file_array[STDIN].inode_ptr = NULL;
	curr_task->file_array[STDIN].file_pos = 0;
	curr_task->file_array[STDIN].flags = 1;

	//setting stdout
	curr_task->file_array[STDOUT].file_op = &terminal_table;
	curr_task->file_array[STDOUT].inode_ptr = NULL;
	curr_task->file_array[STDOUT].file_pos = 0;
	curr_task->file_array[STDOUT].flags = 1;

	uint32_t i;
	for(i = 2; i < PCBNUMFILES; i++)
	{
		curr_task->file_array[i].file_op = NULL;
		curr_task->file_array[i].inode_ptr = NULL;
		curr_task->file_array[i].file_pos = 0;
		curr_task->file_array[i].flags = 0;
	}

	//setting iret context
	tss.ss0 = KERNEL_DS;
	tss.esp0 = task_ebp;

	asm volatile ("cli 							\n 		\
		 movw %0, %%ax 							\n 		\
		 movw %%ax, %%ds 						\n 		\
		 pushl %0								\n 		\
		 pushl %1 								\n 		\
		 pushfl 								\n  	\
		 popl %%eax 							\n 		\
		 orl $0x200, %%eax 						\n 		\
		 pushl %%eax 							\n 		\
		 pushl %2 								\n 		\
		 pushl %3"
		 :
		 :"i"(USER_DS), "i"(TASKSTACK), "i"(USER_CS), "r"(task_eip)
		 :"memory", "cc", "eax");

	asm volatile("iret");

 	asm volatile("halt_ret:");

	return 0;
}

/*
halt
function: halts a program. checks if the program is a shell or not. if so re-runs shell
if not, sets the cur task to the parent task and sets the PID
inputs: 
outputs: 
returns: 0 on success
*/

int32_t halt (uint8_t status)
{
	uint32_t i;
	for (i = 2; i < 8; i++)
	{
		close(i);
	}

	//restoring stack of parent
	tss.esp0 = EIGHTMBADDR - EIGHTKB * (terminals[service_term].curr_PID - 1) - 4;

	asm volatile("movl %0, %%esp    \n 	\
				movl %1, %%ebp"
				:
				:"r"(curr_task->parent_esp), "r"(curr_task->parent_ebp)
				:"memory");

	//checking if halting base shell to restart it
	if(terminals[service_term].curr_PID % 10 == 1){
		curr_task = NULL;
		tss.esp0 = KERNELSTACK;;
		terminals[service_term].curr_pcb = (struct pcb_t*)curr_task;
		terminals[service_term].curr_PID = terminals[service_term].curr_PID - 1;
		clear();
		execute((uint8_t*)"shell");
	}

	curr_task = (pcb_t*)curr_task->parent_task;
	terminals[service_term].curr_pcb = (struct pcb_t*)curr_task;
	terminals[service_term].curr_PID = curr_task->PID;

	//setting paging for parent
	uint32_t task_addr = EIGHTMBADDR + FOURMBADDR * terminals[service_term].curr_PID;
	create_task_page(task_addr);

	asm volatile("jmp halt_ret");

	return 0;
}

/*
getargs
function: copies the args in a pcb to a buffer
inputs: buf , nbytes
outputs: 
returns: 0 on success
*/

int32_t getargs (uint8_t* buf, int32_t nbytes)
{
	if (buf == NULL || nbytes < 0) 
		return -1;
	strncpy((int8_t*)(buf), (int8_t*)(curr_task->args), nbytes);
	return 0;
}

/*
vidmap
function: remaps the video memory to userspace
inputs: screen start
outputs: 
returns: 0 on success
*/

int32_t vidmap (uint8_t** screen_start)
{
	if(screen_start < (uint8_t**) PRGMIMGVADDR || screen_start >= (uint8_t**) PRGMIMGVADDR + FOURMB )
		return -1;

	if (service_term != current_term)
		create_vid_page(term_phys_vid_addr[service_term],0);
	else
		create_vid_page(VIDEOPHYSADDR,0); // the value is 184 * 4kb , the physical address of vid mem
	(*screen_start) = (uint8_t *) (USERVIDEOVIRTADR);
	return 0;
}

/*
set_handler
function:nothing
inputs: signum, ,handler_address
outputs: 
returns: 0
*/

int32_t set_handler (int32_t signum, void* handler_address)
{
	return 0;
}

/*
sigeturn
function:nothing:
inputs: none
outputs: 
returns: 0
*/

int32_t sigreturn (void)
{
	return 0;
}

/*
initShell
function:nothing:
inputs: terminal to initialize shell on
outputs: if it succeeds or not
returns: 0
*/

int32_t initShell(uint32_t term){
	//getting file dentry
	dentry_t temp_dentry;
	if(read_dentry_by_name((uint8_t*)"shell", &temp_dentry) == -1)
		return -1;

	//checking ELF
	uint8_t ELF[4];
	if (read_data(temp_dentry.num_inode, 0, ELF, 4) == -1)
		return -1;

	if (ELF[0] != 0x7F || ELF[1] != 0x45 || ELF[2] != 0x4C || ELF[3] != 0x46)
		return -1;

	terminals[term].curr_PID += 1;

	//setting user page
	uint32_t task_addr = EIGHTMBADDR + FOURMBADDR * terminals[term].curr_PID;
	create_task_page(task_addr);

	//copy data to user page
	if (read_data(temp_dentry.num_inode, 0, (uint8_t*)(PRGMIMGVADDR | PRGMIMGVOFFSET), FOURMB - PRGMIMGVOFFSET) == -1)
		return -1;
	if (read_data(temp_dentry.num_inode, 24, (uint8_t*)&shell_eip, 4) == -1)
		return -1;

	//finding new pcb and stack
	prev_task = (pcb_t*)terminals[term].curr_pcb;
	uint32_t task_ebp = EIGHTMBADDR - EIGHTKB * terminals[term].curr_PID - 4;
	curr_task = (pcb_t*)(task_ebp & 0xFFFE000);
	curr_task->curr_ebp = task_ebp;

	//storing parent esp and ebp
	curr_task->PID = terminals[term].curr_PID;
	asm volatile("movl %%esp, %0     \n 	\
				movl %%ebp, %1"
				:"=r"(curr_task->parent_esp), "=r"(curr_task->parent_ebp)
				:
				:"memory");
	curr_task->parent_task = (struct pcb_t *)prev_task;
	terminals[term].curr_pcb = (struct pcb_t*)curr_task;

	//setting stdin
	curr_task->file_array[STDIN].file_op = &terminal_table;
	curr_task->file_array[STDIN].inode_ptr = NULL;
	curr_task->file_array[STDIN].file_pos = 0;
	curr_task->file_array[STDIN].flags = 1;

	//setting stdout
	curr_task->file_array[STDOUT].file_op = &terminal_table;
	curr_task->file_array[STDOUT].inode_ptr = NULL;
	curr_task->file_array[STDOUT].file_pos = 0;
	curr_task->file_array[STDOUT].flags = 1;

	uint32_t i;
	for(i = 2; i < PCBNUMFILES; i++)
	{
		curr_task->file_array[i].file_op = NULL;
		curr_task->file_array[i].inode_ptr = NULL;
		curr_task->file_array[i].file_pos = 0;
		curr_task->file_array[i].flags = 0;
	}

	//move to stack of new shell
	tss.ss0 = KERNEL_DS;
	tss.esp0 = task_ebp;

	asm volatile("movl %0,  %%esp   \n 	\
				movl %0,	 %%ebp"
				:
				:"r"(task_ebp)
				:"memory");

	//setting iret context
	asm volatile ("movw %0, %%ax 				\n 		\
		 movw %%ax, %%ds 						\n 		\
		 pushl %0								\n 		\
		 pushl %1 								\n 		\
		 pushfl 							\n  	\
		 popl %%eax 							\n 		\
		 orl $0x200, %%eax 						\n 		\
		 pushl %%eax 							\n 		\
		 pushl %2 								\n 		\
		 pushl %3"
		 :
		 :"i"(USER_DS), "i"(TASKSTACK), "i"(USER_CS), "r"(shell_eip)
		 :"memory", "cc", "eax");

	//setting pit return stack
	asm volatile("movl %%esp, %0     \n 	\
				movl %%ebp, %1"
				:"=r"(curr_task->curr_esp), "=r"(curr_task->curr_ebp)
				:
				:"memory");

	//return to kernel stack
	tss.esp0 = KERNELSTACK;

	asm volatile("movl %0, %%esp    \n 	\
				movl %1, %%ebp"
				:
				:"r"(curr_task->parent_esp), "r"(curr_task->parent_ebp)
				:"memory");

	return 0;
}
