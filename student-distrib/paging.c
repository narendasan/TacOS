/* paging.c - Set up page directory, page table and page
 * vim:ts=4 noexpandtab
 */

#include "paging.h"


uint32_t page_directory[PAGEDIRSIZE] __attribute__((aligned(ALIGNSIZE)));
uint32_t first_page_table[PAGETBLSIZE] __attribute__((aligned(ALIGNSIZE)));
uint32_t vid_page_table[PAGETBLSIZE] __attribute__((aligned(ALIGNSIZE)));


void phys_deep_copy(void * dest, void * src, uint32_t size);

/*
paging_init
function: initializes paging/virtualization
inputs: none
outputs: none
returns: none
*/

void paging_init()
{

	uint32_t i, j;
	for(i = 0; i < PAGEDIRSIZE; i++)
	{
	    // This sets the following flags to the pages:
	    //   Supervisor: Only kernel-mode can access them
	    //   Write Enabled: It can be both read from and written to
	    //   Not Present: The page table is not present
	    page_directory[i] = PAGEDIRINIT; 
	}

	 
	//we will fill all 1024 entries in the table, mapping 4 megabytes
	for(j = 0; j < PAGETBLSIZE; j++)
	{
	    // As the address is page aligned, it will always leave 12 bits zeroed.
	    // Those bits are used by the attributes ;)
		if(j == 184)    // 184 is the addr of video memory
			first_page_table[j] = (j * FOURKB) | PRESENT ;  //map to virtual memory and set present,user,and r/w
		else
	    	first_page_table[j] = (j * FOURKB) | NOTPRESENT; // attributes: supervisor level, read/write, not present.
	}

	// from 0 to 4mb for the video memory
	page_directory[0] = ((unsigned int)first_page_table) | PRESENT; //link directory to the correct page table ,set present bit,r/w,and user 
	// we use index 1 because the first 10 bits of 0x00400000 is 0000000001
	page_directory[1] = FOURMBADDR | FOURMBPAGEPRESENT; //points to a 4mb page set present bit,r/w, and supervisor only and S set to 1 to enable 4mb page

	page_directory[33] = ((unsigned int)vid_page_table) | PRESENT; //chose 33 because i think it is un used

	//loading and enabling paging
	asm volatile("movl %0, %%cr3 \n 	\
			movl %%cr4, %%eax \n 		\
			or $0x00000010, %%eax \n  	\
			movl %%eax, %%cr4 \n       	\
			movl %%cr0, %%eax \n       	\
			or $0x80000000, %%eax \n  	\
			movl %%eax, %%cr0 "        
			:                         
			: "r" (page_directory)                  
			: "memory", "cc", "eax" ); 
}

/*
create_task_page
function: creates a page in the directory for a task
inputs: address
outputs: copies address to memory to use in paging for a task
returns: none
*/

void create_task_page(uint32_t addr)
{
	page_directory[32] = addr | FOURMBPAGEPRESENTUSER; // maps the program addr to the correct page directory 

	asm volatile ("movl %%cr3, %%eax   \n	\
 		 movl %%eax, %%cr3"					
 		: 						
 		: 					
 		: "memory", "cc", "eax");
}

/*
create_vid_page
function: map a new page for video memory given a current task
inputs: address
outputs: copies address to memory to use in paging for a video process
returns: none
*/

void create_vid_page(uint32_t addr, uint32_t idx)
{
	vid_page_table[idx] = addr | PRESENT; // map the 4kb page to the phys video memory addr from the 0 to 4mb
	
	asm volatile ("movl %%cr3, %%eax   \n	\
 		 movl %%eax, %%cr3"					
 		: 						
 		: 					
 		: "memory", "cc", "eax");

}

/*
remap_vidmem
function: remaps the kernel video memory to the addr passed in 
inputs: addr
outputs: 
returns: none
*/

void remap_vidmem(uint32_t addr){
	first_page_table[184] = addr | PRESENT; // remap the addr to the kernels userspace

	asm volatile ("movl %%cr3, %%eax   \n	\
 		 movl %%eax, %%cr3"					
 		: 						
 		: 					
 		: "memory", "cc", "eax");
}
