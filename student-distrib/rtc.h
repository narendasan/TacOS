/* rtc.h - Defines for rtc
 * vim:ts=4 noexpandtab
 */

#ifndef _RTC_H
#define _RTC_H

#include "types.h"
#include "lib.h"

/* Ports that rtc sits on */
#define RTC_PORT 0x70
#define RTC_CMD RTC_PORT
#define RTC_DATA RTC_PORT + 1

//chp 2
#define reg_a 0x8A
#define reg_b 0x8B
#define turn_on 0x40
#define mask 0xF0
#define mask2 0x7F


#define hz_2 2
#define hz_4 4
#define hz_8 8
#define hz_16 16
#define hz_32 32
#define hz_64 64
#define hz_128 128
#define hz_256 256
#define hz_512 512
#define hz_1024 1024

#define rate_15 15
#define rate_14 14
#define rate_13 13
#define rate_12 12
#define rate_11 11
#define rate_10 10
#define rate_9 9
#define rate_8 8
#define rate_7 7
#define rate_6 6

void rtc_check();
void rtc_init();

//file operations
int32_t rtc_open(const uint8_t* f_name);
int32_t rtc_read(int32_t fd, void* buf , int32_t nbytes);
int32_t rtc_write(int32_t fd, const void* buf , int32_t nbytes);
int32_t rtc_close(int32_t fd);


#endif /* _RTC_H */

