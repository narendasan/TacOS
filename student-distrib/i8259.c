/* i8259.c - Functions to interact with the 8259 interrupt controller
 * vim:ts=4 noexpandtab
 */

#include "i8259.h"
#include "lib.h"

/* Interrupt masks to determine which interrupts
 * are enabled and disabled */
uint8_t master_mask; /* IRQs 0-7 */
uint8_t slave_mask; /* IRQs 8-15 */

/* Initialize the 8259 PIC */
void
i8259_init(void)
{
    //sending ICWs to initialize PICs
    outb(ICW1,MASTER_8259_CMD);
    outb(ICW1,SLAVE_8259_CMD);

    outb(ICW2_MASTER,MASTER_8259_DATA);
    outb(ICW2_SLAVE,SLAVE_8259_DATA);

    outb(ICW3_MASTER,MASTER_8259_DATA);
    outb(ICW3_SLAVE,SLAVE_8259_DATA);

    outb(ICW4,MASTER_8259_DATA);
    outb(ICW4,SLAVE_8259_DATA);

    //initalize masks
    master_mask = 0xFF;
    slave_mask = 0xFF;
    outb(master_mask,MASTER_8259_DATA);
    outb(slave_mask,SLAVE_8259_DATA);
}

/* Enable (unmask) the specified IRQ
* Params irq flag
* returns NONE */
void
enable_irq(uint32_t irq_num)
{
    //error handling
    if (irq_num > 16 || irq_num < 0){
        printf("INVALID IRQ\n");
        return;
    }

    //setting mask for master
    if(irq_num < 8){
        master_mask &= ~(1 << irq_num);
        outb(master_mask,MASTER_8259_DATA);
    }
    //setting mask for slave
    else {
        irq_num -= 8;
        slave_mask &= ~(1 << irq_num);
        outb(slave_mask,SLAVE_8259_DATA);
        enable_irq(2);
    }
}

/* Disable (mask) the specified IRQ */
void
disable_irq(uint32_t irq_num)
{
    //error handling
    if (irq_num > 15 || irq_num < 0){
        printf("INVALID IRQ\n");
        return;
    }

    //setting mask for master
    if(irq_num < 8){
        master_mask |= (1 << irq_num);
        outb(master_mask,MASTER_8259_DATA);
    }
    //setting mask for slave
    else {
        irq_num -= 8;
        slave_mask |= (1 << irq_num);
        outb(slave_mask,SLAVE_8259_DATA);
        if (slave_mask == 0xFF)
            disable_irq(2);
    }
}

/* Send end-of-interrupt signal for the specified IRQ
* Params irq flag
* returns NONE */
void
send_eoi(uint32_t irq_num)
{
    //error handling
    if (irq_num > 15 || irq_num < 0){
        printf("INVALID IRQ\n");
        return;
    }
    //If in the master pic only need to service master
    if (irq_num < 8){
        outb((EOI|irq_num),MASTER_8259_CMD);
    }
    //If in the slave must service both the master and the slave
    else{
        outb(EOI|(irq_num-8),SLAVE_8259_CMD);
        outb(EOI|2,MASTER_8259_CMD);
    }
}
