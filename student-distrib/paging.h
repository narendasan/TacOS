/* paging.c - Defines for page directory, page table and page
 * vim:ts=4 noexpandtab
 */

#ifndef PAGING_H
#define PAGING_H

#include "types.h"
#include "terminal.h"
#include "lib.h"
 
#define PAGEDIRSIZE 1024
#define PAGETBLSIZE 1024
#define ALIGNSIZE 4096
#define FOURKB 					0x00001000
#define EIGHTKB					0x00002000
#define FOURMB 					0x00400000
#define PAGEDIRINIT  			0x00000002
#define PRESENT 				0x00000007
#define NOTPRESENT 				0x00000002
#define FOURMBPAGEPRESENT 		0x00000083
#define FOURMBPAGEPRESENTUSER 	0x00000087
#define FOURMBADDR  			0x00400000
#define EIGHTMBADDR 			0x00800000
#define VIDEOPHYSADDR           0x000B8000 
#define USERVIDEOVIRTADR        0x08400000


void paging_init();
void create_task_page(uint32_t addr);
void create_vid_page(uint32_t addr, uint32_t idx);
void remap_vidmem(uint32_t addr);

#endif
