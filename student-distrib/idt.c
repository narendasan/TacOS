/* idt.c - Set up interrupt descriptor table
 * vim:ts=4 noexpandtab
 */

#include "idt.h"
#include "lib.h"
#include "x86_desc.h"
#include "exception.h"
#include "asmlink.h"
#include "i8259.h"

/*
 * function: idt_init
 * params: none
 * returns: none
 * initializes Interrupt Descriptor Table by setting entries from exception.c into the IDT
*/
void idt_init()
{
	//template used for each idt entry
	idt_desc_t template;
	template.seg_selector = KERNEL_CS;
	template.present = 1;
	template.dpl = 0;
	template.reserved0 = 0;
	template.size = 1;
	template.reserved1 = 1;
	template.reserved2 = 1;
	template.reserved3 = 1;
	template.reserved4 = 0;

	//setting exceptions
	SET_IDT_ENTRY(template, de_exception);
	idt[0] = template;

	SET_IDT_ENTRY(template, db_exception);
	idt[1] = template;

	SET_IDT_ENTRY(template, nmi_exception);
	idt[2] = template;

	SET_IDT_ENTRY(template, bp_exception);
	idt[3] = template;

	SET_IDT_ENTRY(template, of_exception);
	idt[4] = template;

	SET_IDT_ENTRY(template, br_exception);
	idt[5] = template;

	SET_IDT_ENTRY(template, ud_exception);
	idt[6] = template;

	SET_IDT_ENTRY(template, nm_exception);
	idt[7] = template;

	SET_IDT_ENTRY(template, df_exception);
	idt[8] = template;

	SET_IDT_ENTRY(template, rs_exception);
	idt[9] = template;

	SET_IDT_ENTRY(template, ts_exception);
	idt[10] = template;

	SET_IDT_ENTRY(template, np_exception);
	idt[11] = template;

	SET_IDT_ENTRY(template, ss_exception);
	idt[12] = template;

	SET_IDT_ENTRY(template, gp_exception);
	idt[13] = template;

	SET_IDT_ENTRY(template, pf_exception);
	idt[14] = template;

	SET_IDT_ENTRY(template, rs_exception);
	idt[15] = template;

	SET_IDT_ENTRY(template, mf_exception);
	idt[16] = template;

	SET_IDT_ENTRY(template, ac_exception);
	idt[17] = template;

	SET_IDT_ENTRY(template, mc_exception);
	idt[18] = template;

	SET_IDT_ENTRY(template, xf_exception);
	idt[19] = template;

	//setting reserved
	uint32_t i;
	for (i = 20; i < 32; i++)
	{
		SET_IDT_ENTRY(template, rs_exception);
		idt[i] = template;
	}

	//setting interrupts
	//template.reserved3 = 0;

	SET_IDT_ENTRY(template, keyboard_handler);
	idt[33] = template;
	enable_irq(1);

	SET_IDT_ENTRY(template, pit_handler);
	idt[32] = template;
	enable_irq(0);

	SET_IDT_ENTRY(template, rtc_handler);
	idt[40] = template;
	enable_irq(8);

	//settting system call
	template.dpl =3;
	SET_IDT_ENTRY(template, syscall_handler);
	idt[128] = template;

	//loading interrupt
	lidt(idt_desc_ptr);
}
