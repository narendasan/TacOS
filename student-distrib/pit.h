/* pit.c - Defines for pit
 * vim:ts=4 noexpandtab
 */
#ifndef _PIT_H
#define _PIT_H

#include "types.h"
#include "terminal.h"
#include "keyboard.h"
#include "scheduling.h"
#include "lib.h"

#define DATAPORT 0x40
#define COMMANDPORT 0x43

void pit_init();
void pit_check();

#endif /* _PIT_H */
