/* pit.c - Set up pit
 * vim:ts=4 noexpandtab
 */
#include "pit.h"

#define PIT_CONST 1193180
#define PIT_HZ 100

static uint32_t wait = 0;

/*
pit_init
function: sets tje pit to interrupt at a certain frequency
inputs: 
outputs: 
returns: none
*/

void pit_init()
{
	uint32_t divisor = PIT_CONST / PIT_HZ;
    outb(0x36, COMMANDPORT);
    uint32_t low = divisor & 0xFF;
    outb(low, DATAPORT);
    uint32_t high = divisor >> 8;
    outb(high, DATAPORT);
}

/*
pit_check
function: the handler for the pit and calls scheduling 
inputs: 
outputs: 
returns: none
*/

void pit_check()
{
	if (!wait)
	{
		wait++;
		return;
	}
	scheduling();
}
