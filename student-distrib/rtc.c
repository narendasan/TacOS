/* rtc.c- Set up rtc
 * vim:ts=4 noexpandtab
 */

#include "rtc.h"

volatile uint32_t term_one_flag;
volatile uint32_t term_two_flag;
volatile uint32_t term_three_flag;

/* Function rtc_init
	 params: None
	 returns: None
	 INITALIZE THE RTC
*/

void rtc_init()
{

	outb(reg_b,RTC_CMD);
	char prev=inb(RTC_DATA);
	outb(reg_b,RTC_CMD);
	outb(prev|turn_on,RTC_DATA);

	prev = inb(RTC_CMD);
    outb(prev & mask2,RTC_CMD);

    int rate = rate_15;
    outb(reg_a,RTC_CMD);
	prev=inb(RTC_DATA);
	outb(reg_a,RTC_CMD);
	outb((prev & mask) | rate, RTC_DATA);

	prev = inb(RTC_CMD);
    outb(prev & mask2,RTC_CMD);
}

/* Function rtc_open
	 params: file name
	 returns: 0
	 Opens RTC, sets default rate, and calls rtc_write
*/

int32_t rtc_open(const uint8_t* f_name)
{
	//set the rate to default 2hz when opened
	int32_t fd;
	void* buf;
	int startrate;
	startrate = 2;
	buf = &startrate;
	rtc_write(fd , buf, 1); //just call write with 2 in the buf

	return 0;

}

/* Function rtc_read
	 params: file descriptor, buffer, number of bytes to be read
	 returns: 0
	 Sets interrupt flag and waits until cleared
*/

int32_t rtc_read(int32_t fd, void* buf , int32_t nbytes)
{
	switch (service_term)
	{
		case 0:
			term_one_flag = 1;
			while(term_one_flag);
			break;
		case 1:
			term_two_flag = 1;
			while(term_two_flag);
			break;
		case 2:
			term_three_flag = 1;
			while(term_three_flag);
			break;
		default:
			break;
	}
	return 0;
}

/* Function rtc_write
	 params: file descriptor, buffer, 4 byte int specifying rate
	 returns: 0
	 Sets RTC rate based on nbytes
*/

int32_t rtc_write(int32_t fd, const void* buf , int32_t nbytes)    // 4-byte integer specifying interrupt rate
{
	//check if a 4-byte integer is passed
	if(nbytes!= 4)
		return -1;

	if(buf == NULL)
		return -1;

	//freq = 32768 >>(rate_x - 1)
	int rate;
	switch(*((int*)buf)) {

		case hz_2:
		rate = rate_15;
		break;

		case hz_4:
		rate = rate_14;
		break;

		case hz_8:
		rate = rate_13;
		break;

		case hz_16:
		rate = rate_12;
		break;

		case hz_32:
		rate = rate_11;
		break;

		case hz_64:
		rate = rate_10;
		break;

		case hz_128:
		rate = rate_9;
		break;

		case hz_256:
		rate = rate_8;
		break;

		case hz_512:
		rate = rate_7;
		break;

		case hz_1024:
		rate = rate_6;
		break;

		default:
		return -1;     //ret on failure

	}

	rate &= 0x0F;               //set the rate based on osdev
	outb(reg_a, RTC_CMD);
	char prev = inb(RTC_DATA);
	outb(reg_a, RTC_CMD);
	outb( (prev & mask) | rate ,  RTC_DATA);

	//printf("%d",rate);
	return 0;
}

/*
rtc_close
function: returns 0
inputs: fd
outputs:
returns: 0
*/

int32_t rtc_close(int32_t fd)
{
	return 0;
}

/*
rtc_check
function: toggles the intr flag
inputs:
outputs:
returns:
*/

void rtc_check()
{

	//test_interrupts();
	term_one_flag = 0;
	term_two_flag = 0;
	term_three_flag = 0;
	// switch (service_term)
	// {
	// 	case 0:
	// 		term_one_flag = 0;
	// 		break;
	// 	case 1:
	// 		term_two_flag = 0;
	// 		break;
	// 	case 2:
	// 		term_three_flag = 0;
	// 		break;
	// 	default:
	// 		break;
	// }

	outb(0x0C,RTC_CMD);
	inb(RTC_DATA);
}
