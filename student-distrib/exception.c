/* exception.c - Set up exceptions
 * vim:ts=4 noexpandtab
 */

#include "exception.h"

/*
 * function: de_exception
 * params: none
 * returns: none
 * Prints a divide by 0 exception and halts the program 
 */ 

void de_exception()
{
	clear();
	//cli();
	printf("Divide By 0 Exception\n");
	halt(-1);
}

/*
 * function: db_exception
 * params: none
 * returns: none
 * Prints a debug exception and halts the program 
 */ 

void db_exception()
{
	clear();
	//cli();
	printf("Debug Exception\n");
	halt(-1);
}

/*
 * function: nmi_exception
 * params: none
 * returns: none
 * Prints an NMI (Non Maskable Interrupt) exception and halts the program 
 */ 

void nmi_exception()
{
	clear();
	//cli();
	printf("NMI Exception\n");
	halt(-1);
}

/*
 * function: bp_exception
 * params: none
 * returns: none
 * Prints a breakpoint exception and halts the program 
 */ 

void bp_exception()
{
	clear();
	//cli();
	printf("Breakpoint Exception\n");
	halt(-1);
}

/*
 * function: of_exception
 * params: none
 * returns: none
 * Prints an overflow exception and halts the program 
 */ 

void of_exception()
{
	clear();
	//cli();
	printf("Overflow Exception\n");
	halt(-1);
}

/*
 * function: br_exception
 * params: none
 * returns: none
 * Prints a bound range exceeded exception and halts the program 
 */ 

void br_exception()
{
	clear();
	//cli();
	printf("Bound Range Exceeded Exception\n");
	halt(-1);
}

/*
 * function: ur_exception
 * params: none
 * returns: none
 * Prints an invalid opcode exception and halts the program 
 */ 

void ud_exception()
{
	clear();
	//cli();
	printf("Invalid Opcode Exception\n");
	halt(-1);
}

/*
 * function: nm_exception
 * params: none
 * returns: none
 * Prints a device not available exception and halts the program 
 */ 

void nm_exception()
{
	clear();
	//cli();
	printf("Device Not Available Exception\n");
	halt(-1);
}

/*
 * function: df_exception
 * params: none
 * returns: none
 * Prints a double fault exception and halts the program 
 */ 

void df_exception()
{
	clear();
	//cli();
	printf("Double Fault Exception\n");
	halt(-1);
}

/*
 * function: ts_exception
 * params: none
 * returns: none
 * Prints an invalid TSS exception exception and halts the program 
 */ 

void ts_exception()
{
	clear();
	//cli();
	printf("Invalid TSS Exception\n");
	halt(-1);
}

/*
 * function: np_exception
 * params: none
 * returns: none
 * Prints a segment not present exception and halts the program 
 */ 

void np_exception()
{
	clear();
	//cli();
	printf("Segment Not Present\n");
	halt(-1);
}

/*
 * function: ss_exception
 * params: none
 * returns: none
 * Prints a stack fault exception and halts the program 
 */ 

void ss_exception()
{
	clear();
	//cli();
	printf("Stack Fault Exception\n");
	halt(-1);
}

/*
 * function: gp_exception
 * params: none
 * returns: none
 * Prints a general protection exception and halts the program 
 */ 

void gp_exception()
{
	clear();
	//cli();
	printf("General Protection Exception\n");
	halt(-1);
}

/*
 * function: pf_exception
 * params: none
 * returns: none
 * Prints a page fault exception and halts the program 
 */ 

void pf_exception()
{
	clear();
	//cli();
	printf("Page-Fault Exception\n");
    int addr;
	asm volatile("movl %%cr2, %0"
				:"=r"(addr)
				:
				:"memory");
	printf("%x\n",addr);
	halt(-1);
}

/*
 * function: mf_exception
 * params: none
 * returns: none
 * Prints a x87 FPU Floating-Point error and halts the program 
 */ 

void mf_exception()
{
	clear();
	//cli();
	printf("x87 FPU Floating-Point Error\n");
	halt(-1);
}

/*
 * function: ac_exception
 * params: none
 * returns: none
 * Prints an alignment check exception and halts the program 
 */ 

void ac_exception()
{
	clear();
	//cli();
	printf("Alignment Check Exception\n");
	halt(-1);
}

/*
 * function: mc_exception
 * params: none
 * returns: none
 * Prints a machine check exception and halts the program 
 */ 

void mc_exception()
{
	clear();
	//cli();
	printf("Machine-Check Exception\n");
	halt(-1);
}

/*
 * function: xf_exception
 * params: none
 * returns: none
 * Prints a SIMD Floating-Point exception and halts the program 
 */ 

void xf_exception()
{
	clear();
	//cli();
	printf("SIMD Floating-Point Exception\n");
	halt(-1);
}

/*
 * function: rs_exception
 * params: none
 * returns: none
 * Prints a reserved exception and halts the program 
 */ 

void rs_exception()
{
	clear();
	//cli();
	printf("Reserved Exception\n");
	halt(-1);
}
